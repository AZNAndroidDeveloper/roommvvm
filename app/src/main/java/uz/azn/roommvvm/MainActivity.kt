package uz.azn.roommvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.roommvvm.presentation.IntroFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.frame_layout,IntroFragment()).commit()
    }
}