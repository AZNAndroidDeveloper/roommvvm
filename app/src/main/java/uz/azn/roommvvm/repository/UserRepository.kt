package uz.azn.roommvvm.repository

import android.content.Context
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import uz.azn.roommvvm.dao.database.UserDatabase
import uz.azn.roommvvm.model.User

class UserRepository {
    companion object {
        private var userDB: UserDatabase? = null
        fun initDatabase(context: Context): UserDatabase {
            return UserDatabase.getInstance(context) // bu bizga databaseni yaratib beradi
        }

        fun insert(context: Context, user: User) {
            userDB = initDatabase(context)
            CoroutineScope(IO).launch {
                userDB?.userDao()?.insert(user)
            }

        }

        fun getAllUser(context: Context): LiveData<List<User>> {
            userDB = initDatabase(context)
            return userDB?.userDao()?.getAllUser()!!
        }
    }
}