package uz.azn.roommvvm.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id :Int,
    @ColumnInfo(name = "username")
    val username :String,
    @ColumnInfo(name = "age")
    val age:Int
)
