package uz.azn.roommvvm.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import uz.azn.roommvvm.model.User

@Dao
interface UserDao {

    @Insert() // bu yangi variantni oladi va eskisini ochiradi
   suspend fun insert(user: User)

    @Query("SELECT *FROM user_table ORDER BY id DESC")
    fun  getAllUser():LiveData<List<User>>

    /**
     * getAllUser suspend fun bola olmaydi chunki u har doim kuzatib turadi  backgroud ishlaydi
     * */
}