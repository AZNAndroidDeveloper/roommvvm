package uz.azn.roommvvm.dao.database

import android.content.Context
import androidx.room.Database
import androidx.room.Entity
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.azn.roommvvm.dao.UserDao
import uz.azn.roommvvm.model.User

@Database(entities = [User::class],version = 1, exportSchema = false) //  exportSchema bu eskisini ochirib yangisni oladi shuning uchun ishlatiladi
abstract class UserDatabase:RoomDatabase() {
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: UserDatabase? = null
        fun getInstance(context: Context): UserDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext, UserDatabase::class.java, "user_db"
                    )
                        .fallbackToDestructiveMigrationFrom()
                        .build()
                    INSTANCE = instance

                }
                return instance

            }
        }
    }
}