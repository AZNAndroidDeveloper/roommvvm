package uz.azn.roommvvm.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.roommvvm.model.User
import uz.azn.roommvvm.databinding.IntroViewHolderBinding as IntroBinding

class IntroAdapter ():RecyclerView.Adapter<IntroAdapter.IntroViewHolder>(){
    private val elements  = mutableListOf<User>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroViewHolder {
return IntroViewHolder(IntroBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int  = elements.size

    override fun onBindViewHolder(holder: IntroViewHolder, position: Int) {
holder.onBind(elements[position])
    }
    inner class IntroViewHolder(private val binding:IntroBinding):RecyclerView.ViewHolder(binding.root){
        fun onBind(element:User){
            with(binding){
                tvAge.text = element.age.toString()
                tvUserName.text = element.username
            }
        }
    }

    fun setData(element:List<User>){
        elements.clear()
        elements.addAll(element)
        notifyDataSetChanged()
    }

}