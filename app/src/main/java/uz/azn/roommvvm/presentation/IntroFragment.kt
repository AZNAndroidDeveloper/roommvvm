package uz.azn.roommvvm.presentation

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.roommvvm.R
import uz.azn.roommvvm.databinding.FragmentIntroBinding
import uz.azn.roommvvm.model.User
import uz.azn.roommvvm.viewModel.IntroViewModel


class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private lateinit var viewModel: IntroViewModel
    private lateinit var builder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog
    private var introAdapter = IntroAdapter()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        viewModel = ViewModelProvider(requireActivity()).get(IntroViewModel::class.java)

        viewModel.getAllData(requireContext()).observe(requireActivity(), Observer {
            introAdapter.setData(it)
        })
        introAdapter.setData(ArrayList<User>())



        with(binding) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = introAdapter

            }
            btnAdd.setOnClickListener {
                showInsertDialog()
            }
        }
    }

    private fun showInsertDialog() {
        builder = AlertDialog.Builder(requireContext())
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.insert_user_dialog, null)
        builder.setView(view)
        val etUsername = view.findViewById<EditText>(R.id.et_user_name)
        val etAge = view.findViewById<EditText>(R.id.et_age)
        val btnSave = view.findViewById<Button>(R.id.btn_save)

        dialog = builder.create()
        btnSave.setOnClickListener {

            if (etAge.text.isNotEmpty() && etUsername.text.isNotEmpty()) {
                val user = User(0, etUsername.text.toString(), etAge.text.toString().toInt())

                saveDatabase(user)
                dialog.dismiss()
            } else {
                Toast.makeText(requireContext(), "Malumotni to'liq kiriting", Toast.LENGTH_SHORT)
                    .show()
            }
        }



        dialog.show()
    }

    private fun saveDatabase(user: User) {
        viewModel.insert(requireContext(), user)
    }
}