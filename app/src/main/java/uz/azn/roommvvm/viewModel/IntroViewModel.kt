package uz.azn.roommvvm.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import uz.azn.roommvvm.model.User
import uz.azn.roommvvm.repository.UserRepository

class IntroViewModel :ViewModel(){
    fun insert(context: Context,user: User){
        UserRepository.insert(context,user)

    }
    fun getAllData(context: Context):LiveData<List<User>>{
       return UserRepository.getAllUser(context)
    }
}